# KarmaModules Twitter Feed #
Documentación completa del módulo [aquí](https://karmapulse.atlassian.net/wiki/spaces/KMod/pages/68550659/KarmaModule+Twitter+Feed)

### Instalación para desarrollo ###
```
yarn install
```

### Correr demo en desarrollo ###
```
yarn start
```

### Para la implementación ###
```
yarn add https://git@bitbucket.org/karmapulse/kmodules_tw_feed.git
```

En el archivo donde se requiera:
```
import TwitterFeed from 'kmodules_tw_feed';
```
