import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Error,
    Empty,
    FeedList,
    FeedListLoading,
} from 'visualizations';

import styles from './styles';
import { defaultData, defaultConfig } from './defaultValues';

const propTypes = {
    moduleConfig: PropTypes.shape({
        visualization: PropTypes.string,
        tweet_types: PropTypes.arrayOf(PropTypes.string),
        load_type: PropTypes.string,
        date_format: PropTypes.string,
        show_user_info: PropTypes.bool,
        show_action_buttons: PropTypes.bool,
        show_sentiment: PropTypes.bool,
        show_counts: PropTypes.bool,
        count_format: PropTypes.string,
        show_media: PropTypes.bool,
        active_autoplay: PropTypes.bool
    }),
    moduleData: PropTypes.shape({
        total: PropTypes.number,
        data: PropTypes.array
    }),
    moduleColor: PropTypes.string,
    moduleFetch: PropTypes.func
};

const defaultProps = {
    moduleConfig: defaultConfig,
    moduleData: defaultData,
    moduleColor: '#555',
    moduleFetch: () => {}
};

class TwitterFeed extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: props.moduleConfig,
            moduleData: props.moduleData,
            moduleColor: props.moduleColor,
            moduleFetch: props.moduleFetch
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            moduleConfig: nextProps.moduleConfig,
            moduleData: nextProps.moduleData,
            moduleColor: nextProps.moduleColor,
            moduleFetch: nextProps.moduleFetch
        });
    }

    renderByViz() {
        const { moduleData, moduleConfig } = this.state;

        if (moduleConfig && moduleData) {
            if (moduleData.data.length >= 0) {
                const visualizations = {
                    feed_list: params => (
                        <FeedList {...params} />
                    )
                };

                return visualizations[moduleConfig.visualization](this.state);
            }
            return <Empty />;
        }

        return <FeedListLoading />;
    }

    render() {
        return (
            <div {...styles()}>
                {this.renderByViz()}
            </div>
        );
    }
}

TwitterFeed.propTypes = propTypes;
TwitterFeed.defaultProps = defaultProps;

export default TwitterFeed;
