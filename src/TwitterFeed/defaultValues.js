// Para referencia, consultar:
// https://karmapulse.atlassian.net/wiki/spaces/KMod/pages/68550659/KarmaModule+Twitter+Feed

const defaultConfig = {
    visualization: 'feed_list',
    tweet_types: ['replies', 'retweets', 'quotes', 'originals'],
    load_type: 'infinite_scroll',
    date_format: 'time_stamp',
    show_user_info: false,
    show_action_buttons: true,
    show_sentiment: false,
    show_counts: false,
    count_format: 'count_abbreviation',
    show_media: true,
    active_autoplay: false
};

const defaultData = {
    total: 5,
    data: [
        {
            sort: [
                1513617687000
            ],
            _type: 'tweet',
            _source: {
                body: 'Con 3 encuestas, #PRD "medirá" a candidatos para la #CdMx https://t.co/fEABPTopYE',
                urls: [
                    {
                        url: 'http://www.milenio.com/elecciones-mexico-2018/prd-cdmx-candidato-alejandra-barrales-chertorivski-ahued-flores-eleccion-mitofsky_0_1087091312.html?utm_source=Twitter&utm_medium=Referral&utm_term=Politica&utm_campaign=Milenio',
                        description: 'El líder local del PRD, Raúl Flores, informó que los resultados de las encuestas que se realicen servirán como apoyo a los consejeros del partido para definir al candidato a la Jefatura de Gobierno.',
                        title: 'Con 3 encuestas, PRD "medirá" a candidatos para la CdMx'
                    }
                ],
                hashtags: [
                    'PRD',
                    'CdMx'
                ],
                classificators: [
                    {
                        tag: 'neutral',
                        classificator: 'classifier_spanish_politics_sentiment'
                    }
                ],
                language: 'es',
                images: [
                    {
                        url: 'http://pbs.twimg.com/media/DRWF3o9VoAAN9CU.jpg',
                        id: '942806887937974272'
                    }
                ],
                engagement: 12,
                have_urls: true,
                connection_id: '5a4e86ad496b17000e767c7a',
                rule_id: '5a4e8634496b17000e767c79',
                application: 'TweetDeck',
                verb: 'post',
                posted_time: '2017-12-18T17:21:27.000Z',
                user: {
                    username: 'Milenio',
                    verified: true,
                    name: 'Milenio.com',
                    language: 'es',
                    image: 'https://pbs.twimg.com/profile_images/947092874763440128/G66CyhRV_normal.jpg',
                    sumarry: '#FuerzaMéxico 🇲🇽',
                    topics: [
                        {
                            topic_type: 'influence',
                            score: 100,
                            displayName: 'Krav Maga',
                            id: '9219221220892056523'
                        },
                        {
                            topic_type: 'influence',
                            score: 100,
                            displayName: 'Gisele Bundchen',
                            id: '9219221220892057219'
                        },
                        {
                            topic_type: 'influence',
                            score: 100,
                            displayName: 'Sylvester Stallone',
                            id: '139'
                        },
                        {
                            topic_type: 'interest',
                            score: 100,
                            displayName: 'Mexico',
                            id: '6012786567441837391'
                        },
                        {
                            topic_type: 'interest',
                            score: 98,
                            displayName: 'News and Media',
                            id: '10000000000000000041'
                        },
                        {
                            topic_type: 'interest',
                            score: 82,
                            displayName: 'Spain',
                            id: '5269859547756677502'
                        },
                        {
                            topic_type: 'interest',
                            score: 81,
                            displayName: 'Film',
                            id: '7648257024411397825'
                        },
                        {
                            topic_type: 'interest',
                            score: 75,
                            displayName: 'Religion',
                            id: '10000000000000000022'
                        }
                    ],
                    link: 'http://www.twitter.com/Milenio',
                    location: 'México',
                    klout: 79,
                    posted_time: '2009-03-17T22:26:38.000Z',
                    followers: 4030363,
                    id: '24969337'
                },
                favorites: 7,
                media_type: 'image',
                received_at: '2018-01-04T19:55:32.471Z',
                link: 'http://twitter.com/Milenio/statuses/942807032343736320',
                type: 'original',
                id: '942807032343736320',
                retweets: 5
            },
            _score: null,
            _index: 'tweets_2018-01',
            _id: '9428070323437363205a4e8634496b17000e767c79'
        },
        {
            sort: [
                1513612276000
            ],
            _type: 'tweet',
            _source: {
                body: 'Estimados lectores. En la portada impresa de hoy se atribuye la autoría de nuestra encuesta de hoy a la encuestadora Buendia &amp; Laredo. Aclaramos que la elaboración del estudio corresponde sólo a El Universal.',
                classificators: [
                    {
                        tag: 'neutral',
                        classificator: 'classifier_spanish_politics_sentiment'
                    }
                ],
                language: 'es',
                engagement: 7,
                connection_id: '5a4e86ad496b17000e767c7a',
                rule_id: '5a4e8634496b17000e767c79',
                application: 'Twitter Web Client',
                verb: 'post',
                posted_time: '2017-12-18T15:51:16.000Z',
                user: {
                    username: 'El_Universal_Mx',
                    verified: true,
                    name: 'El Universal',
                    language: 'es',
                    image: 'https://pbs.twimg.com/profile_images/947204840244985856/0D8_b8lC_normal.jpg',
                    sumarry: 'Cuenta oficial de El Gran Diario de México. Conoce una amplia variedad de noticias, así como contenidos multimedia.',
                    topics: [
                        {
                            topic_type: 'influence',
                            score: 100,
                            displayName: 'Jalisco',
                            id: '173'
                        },
                        {
                            topic_type: 'influence',
                            score: 100,
                            displayName: 'Ships',
                            id: '5733'
                        },
                        {
                            topic_type: 'interest',
                            score: 100,
                            displayName: 'Mexico',
                            id: '6012786567441837391'
                        },
                        {
                            topic_type: 'interest',
                            score: 96,
                            displayName: 'News and Media',
                            id: '10000000000000000041'
                        },
                        {
                            topic_type: 'interest',
                            score: 85,
                            displayName: 'Spain',
                            id: '5269859547756677502'
                        },
                        {
                            topic_type: 'interest',
                            score: 85,
                            displayName: 'Politics',
                            id: '8119426466902417284'
                        },
                        {
                            topic_type: 'interest',
                            score: 82,
                            displayName: 'Advertising',
                            id: '6136498691401480079'
                        }
                    ],
                    link: 'http://www.twitter.com/El_Universal_Mx',
                    location: 'Centro de la Ciudad de México',
                    klout: 89,
                    posted_time: '2008-10-10T00:09:06.000Z',
                    followers: 4684552,
                    id: '16676396'
                },
                favorites: 4,
                mentions: [
                    'Claudiashein',
                    'Ale_BarralesM'
                ],
                received_at: '2018-01-04T19:55:36.838Z',
                link: 'http://twitter.com/El_Universal_Mx/statuses/942784334481772545',
                type: 'reply',
                id: '942784334481772545',
                child_tweet: {
                    engagement: 0,
                    link: 'http://twitter.com/El_Universal_Mx/statuses/942784334481772545',
                    retweets: 0,
                    favorites: 0,
                    received_at: '2018-01-04T19:55:36.838Z',
                    posted_time: '2017-12-18T15:51:16.000Z',
                    id: '942784334481772545'
                },
                retweets: 3
            },
            _score: null,
            _index: 'tweets_2018-01',
            _id: '9427843344817725455a4e8634496b17000e767c79'
        },
        {
            sort: [
                1513603590000
            ],
            _type: 'tweet',
            _source: {
                body: '#EncuestaElUniversal @Claudiashein aventaja con 12.3 puntos porcentuales a @Ale_BarralesM https://t.co/mD4ma5MArX',
                urls: [
                    {
                        url: 'http://www.eluniversal.com.mx/metropoli/cdmx/morena-encabeza-la-intencion-de-voto-en-la-ciudad-de-mexico',
                        description: 'Claudia Sheinbaum aventaja con 12.3 puntos porcentuales a Alejandra Barrales; en lucha por el Frente, la perredista supera a Salomón Chertorivski y Armando Ahued',
                        title: 'Morena encabeza la intención de voto en la Ciudad de México'
                    }
                ],
                hashtags: [
                    'EncuestaElUniversal'
                ],
                classificators: [
                    {
                        tag: 'neutral',
                        classificator: 'classifier_spanish_politics_sentiment'
                    }
                ],
                language: 'es',
                engagement: 35,
                have_urls: true,
                connection_id: '5a4e86ad496b17000e767c7a',
                rule_id: '5a4e8634496b17000e767c79',
                application: 'TweetDeck',
                verb: 'post',
                posted_time: '2017-12-18T13:26:30.000Z',
                user: {
                    username: 'El_Universal_Mx',
                    verified: true,
                    name: 'El Universal',
                    language: 'es',
                    image: 'https://pbs.twimg.com/profile_images/947204840244985856/0D8_b8lC_normal.jpg',
                    sumarry: 'Cuenta oficial de El Gran Diario de México. Conoce una amplia variedad de noticias, así como contenidos multimedia.',
                    topics: [
                        {
                            topic_type: 'influence',
                            score: 100,
                            displayName: 'Jalisco',
                            id: '173'
                        },
                        {
                            topic_type: 'influence',
                            score: 100,
                            displayName: 'Ships',
                            id: '5733'
                        },
                        {
                            topic_type: 'interest',
                            score: 100,
                            displayName: 'Mexico',
                            id: '6012786567441837391'
                        },
                        {
                            topic_type: 'interest',
                            score: 96,
                            displayName: 'News and Media',
                            id: '10000000000000000041'
                        },
                        {
                            topic_type: 'interest',
                            score: 85,
                            displayName: 'Spain',
                            id: '5269859547756677502'
                        },
                        {
                            topic_type: 'interest',
                            score: 85,
                            displayName: 'Politics',
                            id: '8119426466902417284'
                        },
                        {
                            topic_type: 'interest',
                            score: 82,
                            displayName: 'Advertising',
                            id: '6136498691401480079'
                        }
                    ],
                    link: 'http://www.twitter.com/El_Universal_Mx',
                    location: 'Centro de la Ciudad de México',
                    klout: 89,
                    posted_time: '2008-10-10T00:09:06.000Z',
                    followers: 4684552,
                    id: '16676396'
                },
                favorites: 20,
                mentions: [
                    'Claudiashein',
                    'Ale_BarralesM'
                ],
                received_at: '2018-01-04T20:05:33.586Z',
                link: 'http://twitter.com/El_Universal_Mx/statuses/942747905361883137',
                type: 'original',
                id: '942747905361883137',
                retweets: 15
            },
            _score: null,
            _index: 'tweets_2018-01',
            _id: '9427479053618831375a4e8634496b17000e767c79'
        },
        {
            sort: [
                1513559607000
            ],
            _type: 'tweet',
            _source: {
                body: '\'Con todosísimo respeto, sería mejor que se dedique a gobernar\' \n\nPide Sheinbaum pide a @ManceraMiguelMX 😯\nhttps://t.co/h2fSemR3Vx',
                urls: [
                    {
                        url: 'http://www.milenio.com/elecciones-mexico-2018/claudia-sheinbaum-mancera-jefe-gobierno-morena-frente-cdmx-debate-milenio-noticias_0_1086491556.html?utm_source=Twitter&utm_medium=Referral&utm_term=Politica&utm_campaign=Milenio',
                        description: 'La precandidata de Morena a la CdMx, Claudia Sheinbaum, pidió al jefe de Gobierno permitir a los precandidatos del Frente debatir propuestas con ella.',
                        title: 'Sheinbaum pide a Mancera no debatir y dedicarse a trabajar'
                    }
                ],
                have_urls: true,
                classificators: [
                    {
                        tag: 'positivo',
                        classificator: 'classifier_spanish_politics_sentiment'
                    }
                ],
                language: 'es',
                images: [
                    {
                        url: 'http://pbs.twimg.com/media/DRSob_jVoAAgkZw.jpg',
                        id: '942563420896862208'
                    }
                ],
                engagement: 15,
                connection_id: '5a4e86ad496b17000e767c7a',
                media_type: 'image',
                rule_id: '5a4e8634496b17000e767c79',
                application: 'TweetDeck',
                verb: 'post',
                posted_time: '2017-12-18T01:13:27.000Z',
                user: {
                    username: 'Milenio',
                    verified: true,
                    name: 'Milenio.com',
                    language: 'es',
                    image: 'https://pbs.twimg.com/profile_images/947092874763440128/G66CyhRV_normal.jpg',
                    sumarry: '#FuerzaMéxico 🇲🇽',
                    topics: [
                        {
                            topic_type: 'influence',
                            score: 100,
                            displayName: 'Krav Maga',
                            id: '9219221220892056523'
                        },
                        {
                            topic_type: 'influence',
                            score: 100,
                            displayName: 'Gisele Bundchen',
                            id: '9219221220892057219'
                        },
                        {
                            topic_type: 'influence',
                            score: 100,
                            displayName: 'Sylvester Stallone',
                            id: '139'
                        },
                        {
                            topic_type: 'interest',
                            score: 100,
                            displayName: 'Mexico',
                            id: '6012786567441837391'
                        },
                        {
                            topic_type: 'interest',
                            score: 98,
                            displayName: 'News and Media',
                            id: '10000000000000000041'
                        },
                        {
                            topic_type: 'interest',
                            score: 82,
                            displayName: 'Spain',
                            id: '5269859547756677502'
                        },
                        {
                            topic_type: 'interest',
                            score: 81,
                            displayName: 'Film',
                            id: '7648257024411397825'
                        },
                        {
                            topic_type: 'interest',
                            score: 75,
                            displayName: 'Religion',
                            id: '10000000000000000022'
                        }
                    ],
                    link: 'http://www.twitter.com/Milenio',
                    location: 'México',
                    klout: 79,
                    posted_time: '2009-03-17T22:26:38.000Z',
                    followers: 4030363,
                    id: '24969337'
                },
                favorites: 12,
                mentions: [
                    'ManceraMiguelMX'
                ],
                received_at: '2018-01-04T19:55:33.588Z',
                link: 'http://twitter.com/Milenio/statuses/942563427985346560',
                type: 'original',
                id: '942563427985346560',
                retweets: 3
            },
            _score: null,
            _index: 'tweets_2018-01',
            _id: '9425634279853465605a4e8634496b17000e767c79'
        },
        {
            sort: [
                1513542000000
            ],
            _type: 'tweet',
            _source: {
                body: '\'Estamos hartos de que no se gobierne bien, de que los servicios no lleguen a los capitalinos\', dijo  https://t.co/mbLNMbj5UQ',
                urls: [
                    {
                        url: 'http://www.eluniversal.com.mx/elecciones-2018/inicia-mikel-arriola-precampana-en-cuajimalpa',
                        description: '\'Estamos hartos de que no se gobierne bien, de que los servicios no lleguen a los capitalinos\', dijo a los militantes que lo acompañaron en la demarcación',
                        title: 'Inicia Mikel Arriola precampaña en Cuajimalpa'
                    }
                ],
                have_urls: true,
                classificators: [
                    {
                        tag: 'neutral',
                        classificator: 'classifier_spanish_politics_sentiment'
                    }
                ],
                language: 'es',
                engagement: 36,
                connection_id: '5a4e86ad496b17000e767c7a',
                rule_id: '5a4e8634496b17000e767c79',
                application: 'TweetDeck',
                verb: 'post',
                posted_time: '2017-12-17T20:20:00.000Z',
                user: {
                    username: 'El_Universal_Mx',
                    verified: true,
                    name: 'El Universal',
                    language: 'es',
                    image: 'https://pbs.twimg.com/profile_images/947204840244985856/0D8_b8lC_normal.jpg',
                    sumarry: 'Cuenta oficial de El Gran Diario de México. Conoce una amplia variedad de noticias, así como contenidos multimedia.',
                    topics: [
                        {
                            topic_type: 'influence',
                            score: 100,
                            displayName: 'Jalisco',
                            id: '173'
                        },
                        {
                            topic_type: 'influence',
                            score: 100,
                            displayName: 'Ships',
                            id: '5733'
                        },
                        {
                            topic_type: 'interest',
                            score: 100,
                            displayName: 'Mexico',
                            id: '6012786567441837391'
                        },
                        {
                            topic_type: 'interest',
                            score: 96,
                            displayName: 'News and Media',
                            id: '10000000000000000041'
                        },
                        {
                            topic_type: 'interest',
                            score: 85,
                            displayName: 'Spain',
                            id: '5269859547756677502'
                        },
                        {
                            topic_type: 'interest',
                            score: 85,
                            displayName: 'Politics',
                            id: '8119426466902417284'
                        },
                        {
                            topic_type: 'interest',
                            score: 82,
                            displayName: 'Advertising',
                            id: '6136498691401480079'
                        }
                    ],
                    link: 'http://www.twitter.com/El_Universal_Mx',
                    location: 'Centro de la Ciudad de México',
                    klout: 89,
                    posted_time: '2008-10-10T00:09:06.000Z',
                    followers: 4684552,
                    id: '16676396'
                },
                favorites: 24,
                received_at: '2018-01-04T19:55:37.144Z',
                link: 'http://twitter.com/El_Universal_Mx/statuses/942489577062887425',
                type: 'original',
                id: '942489577062887425',
                retweets: 12
            },
            _score: null,
            _index: 'tweets_2018-01',
            _id: '9424895770628874255a4e8634496b17000e767c79'
        }
    ],
};

export { defaultConfig, defaultData };
