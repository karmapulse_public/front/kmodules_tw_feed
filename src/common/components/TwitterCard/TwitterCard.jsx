/*
*   COMPONENTE REUTILIZABLE
*  --------------------------
*   Usar este componente en varias visualizaciones si se requiere.
*/

import React from 'react';
import PropTypes from 'prop-types';
import upperFirst from 'lodash/upperFirst';
import moment from 'moment';
import numberFormatter from 'kmodules_helpers/numberFormatter';
import formatBodyText from 'kmodules_helpers/formatBodyText';

import {
    VerifiedIcon,
    UserIcon,
    LikeIcon,
    ReplyIcon,
    RetweetIcon,
    imageNoUser
} from 'icons';

import styles from './styles';

const propTypes = {
    tweet: PropTypes.object,
    moduleConfig: PropTypes.object,
    children: PropTypes.object
};

const defaultProps = {
    tweet: {},
    moduleConfig: {},
    children: {}
};

const TwitterCard = ({ tweet, moduleConfig, children }) => {
    const { _source } = tweet;
    const body = () => ({ __html: formatBodyText(_source.body) });
    const { user } = _source;
    const dateMoment = upperFirst(moment(_source.posted_time).format('MMM D, YYYY HH:mm'));
    const followers = numberFormatter(user.followers);
    const sentiment = _source.classificators ? upperFirst(_source.classificators[0].tag) : 'ninguno';
    const verified = user.verified ? <VerifiedIcon /> : '';
    const retweets = () => {
        if (_source.retweets) {
            return (
                <div>
                    <h5>Retweets</h5>
                    <h4>{numberFormatter(_source.retweets)}</h4>
                </div>
            );
        }
        return '';
    };
    const likes = () => {
        if (_source.favorites) {
            return (
                <div>
                    <h5>Me gusta</h5>
                    <h4>{numberFormatter(_source.favorites)}</h4>
                </div>
            );
        }
        return '';
    };
    let imgPicture = '';

    const showUserInfo = () => {
        if (moduleConfig.show_user_info) {
            return (
                <div className="container__sub-header">
                    <div className="container__sub-header__kloutscore">
                        <h5>{user.klout}</h5>
                    </div>
                    <div className="container__sub-header__followers">
                        <h5>
                            <UserIcon color="#000" />
                            {followers}
                        </h5>
                    </div>
                </div>
            );
        }

        return '';
    };

    const showActionButtons = () => {
        if (moduleConfig.show_action_buttons) {
            return (
                <div className="container__footer__reactions">
                    <a
                        href={`https://twitter.com/intent/tweet?in_reply_to=${_source.id}`}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <ReplyIcon color="#bdbdbd" />
                    </a>
                    <a
                        href={`https://twitter.com/intent/retweet?tweet_id=${_source.id}`}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <RetweetIcon color="#bdbdbd" />
                    </a>
                    <a
                        href={`https://twitter.com/intent/like?tweet_id=${_source.id}`}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <LikeIcon color="#bdbdbd" />
                    </a>
                </div>
            );
        }

        return '';
    };

    const showCounts = () => {
        if (moduleConfig.show_counts) {
            return (
                <div className="container__footer__counts">
                    { retweets() }
                    { likes() }
                </div>
            );
        }
        return '';
    };

    const showSentiment = () => {
        if (moduleConfig.show_sentiment) {
            return (
                <div className="container__footer__sentiment">
                    <div className={`container__footer__sentiment__oval oval-${sentiment}`} />
                    <h5 className={`text-${sentiment}`}>{sentiment}</h5>
                </div>
            );
        }
        return '';
    };

    return (
        <div {...styles()}>
            <div className="twitter-card">
                <a
                    className="twitter-card__user-image"
                    href={user.link}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <img
                        src={user.image}
                        alt="Foto de usuario"
                        ref={(a) => { imgPicture = a; }}
                        onError={() => { imgPicture.src = imageNoUser; }}
                    />
                </a>
                <div className="twitter-card__container">
                    <div className="container__header">
                        <div>
                            <a
                                className="container__header__name"
                                href={user.link}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                {user.name}
                                {verified}
                            </a>
                            <a
                                className="container__header__username"
                                href={user.link}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                @{user.username}
                            </a>
                        </div>
                        <a
                            className="container__header__datetime"
                            href={_source.link}
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            {dateMoment}
                        </a>
                    </div>
                    {showUserInfo()}
                    <div className="container__body" dangerouslySetInnerHTML={body()} />
                    {children}
                    <div className="container__footer">
                        {showActionButtons()}
                        {showCounts()}
                        {showSentiment()}
                    </div>
                </div>
            </div>
        </div>
    );
};

TwitterCard.propTypes = propTypes;
TwitterCard.defaultProps = defaultProps;

export default TwitterCard;
