import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
    color: PropTypes.string
};

const defaultProps = {
    color: '#000'
};

const UserIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
        <g fill={props.color} fillRule="evenodd">
            <path d="M7 7c1.93 0 3.5-1.57 3.5-3.5S8.93 0 7 0 3.5 1.57 3.5 3.5 5.07 7 7 7M7 7.875c-4.121 0-7 2.159-7 5.25V14h14v-.875c0-3.091-2.879-5.25-7-5.25"/>
        </g>
    </svg>
    /* eslint-enable */
);

UserIcon.propTypes = propTypes;
UserIcon.defaultProps = defaultProps;

export default UserIcon;
