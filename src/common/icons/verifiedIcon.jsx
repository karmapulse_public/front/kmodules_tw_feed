import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
    color: PropTypes.string
};

const defaultProps = {
    color: '#4990E2'
};

const VerifiedIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12">
        <g fill="none" fillRule="evenodd">
            <circle cx="6" cy="6" r="6" fill={props.color} />
            <path fill="#FFF" d="M5.821 8.494L3.2 6.528l.791-1.055 1.597 1.198L8.353 3.2l1.031.822z"/>
        </g>
    </svg>
    /* eslint-enable */
);

VerifiedIcon.propTypes = propTypes;
VerifiedIcon.defaultProps = defaultProps;

export default VerifiedIcon;
