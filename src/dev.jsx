import React from 'react';
import { render } from 'react-dom';
import TwitterFeed from './TwitterFeed';

const MyApp = () => <TwitterFeed />;

render(<MyApp />, document.getElementById('root'));
