import React from 'react';
import TwitterFeed from './TwitterFeed';

const MyApp = props => <TwitterFeed {...props} />;

export default MyApp;
