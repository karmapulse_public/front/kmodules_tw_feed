/*
*   VISUALIZACIÓN SIN DATA
*  --------------------------
*   Usar este componente para indicar que el módulo no tiene data aún.
*/

import React from 'react';
import styles from './styles';

export default () => (
    <div {...styles('#ccc', 'center')}>
        No hay datos
    </div>
);
