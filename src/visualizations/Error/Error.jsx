/*
*   VISUALIZACIÓN DE ERROR
*  --------------------------
*   Usar este componente cuando haya un error en el componente
*/

import React from 'react';
import styles from './styles';

export default () => (
    <div {...styles('#ccc', 'center')}>
        Uuups :( hubo un error
    </div>
);
