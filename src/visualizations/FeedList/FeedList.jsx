import React from 'react';

import { TwitterCard, TwitterGallery } from 'components';
import styles from './styles';

const FeedList = ({ moduleData, moduleConfig }) => {
    const renderTweetList = data => (
        data.map(
            (tweet, index) => (
                <TwitterCard key={index} tweet={tweet} moduleConfig={moduleConfig} >
                    <TwitterGallery items={tweet._source.images} />
                </TwitterCard>
            )
        )
    );

    return (
        <div {...styles('#222', 'left')}>
            {renderTweetList(moduleData.data)}
        </div>
    );
};

export default FeedList;
